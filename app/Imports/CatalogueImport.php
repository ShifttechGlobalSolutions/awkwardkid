<?php

namespace App\Imports;

use App\Models\Catalogue;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CatalogueImport implements ToModel //, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Catalogue([
            'ProductCategory' => $row[0],
            'ProductName' => $row[1],
            'ProductDescription' => $row[2],
            'ProductCode' => $row[3],
            'ProductPrice' => $row[4],
            'ProductOldPrice' => $row[5],
            'ProductDiscount' => $row[6],
            'StockQuantity' => $row[7],
            'file_path' => $row[8],
            'ProductPromotionStatus' => $row[9],
            'productId' => $row[10],
        ]);
    }
}
