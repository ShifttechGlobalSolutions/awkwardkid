<?php

namespace App\Http\Controllers;

use App\Models\SearchProducts;
use Illuminate\Http\Request;

class SearchProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SearchProducts  $searchProducts
     * @return \Illuminate\Http\Response
     */
    public function show(SearchProducts $searchProducts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SearchProducts  $searchProducts
     * @return \Illuminate\Http\Response
     */
    public function edit(SearchProducts $searchProducts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SearchProducts  $searchProducts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SearchProducts $searchProducts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SearchProducts  $searchProducts
     * @return \Illuminate\Http\Response
     */
    public function destroy(SearchProducts $searchProducts)
    {
        //
    }
}
