<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Catalogue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function addToCart(Request $request, $id)
    {
        $quant = 1; //$request->get('quantity');
        $catalogue = Catalogue::find($id);
        if(!$catalogue) {
            abort(404);
        }

        $cart = session()->get('cart');
       
        // if cart is empty then this the first product
        if(!$cart) {
            $cart = [
                    $id => [
                        "name" => $catalogue->ProductName,
                        "description" => $catalogue->ProductDescription,
                        "quantity" => 2,
                        "price" => $catalogue->ProductPrice,
                        "productid" => $catalogue->productid
                        
                    ]
            ];
            session()->put('cart', $cart);
            return redirect('productDetails/'.$id)->with('success', 'Product added to cart successfully!');
        }
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {
            $cart[$id]['quantity']++;
            session()->put('cart', $cart);
            foreach($cart as $key => $c){
                return redirect('productDetails/'.$id)->with('success', 'Product added to cart successfully!');
            }
        }
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "name" => $catalogue->ProductName,
            "description" => $catalogue->ProductDescription,
            "quantity" => 1,
            "price" => $catalogue->ProductPrice,
            "discount" => $catalogue->ProductDiscount,
            "productid" => $catalogue->productid
            
        ];
        session()->put('cart', $cart);
        return redirect('productDetails/'.$id)->with('success', 'Product added to cart successfully!');
       
    }

    public function cartQty(Request $request){
        $cart = session()->get('cart');
        return collect($cart)->count();
    }

    public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');
            $cart[$request->id]["quantity"] = $request->quantity;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }
    public function remove($id)
    {  
        if($id) {
            $cart = session()->get('cart');
            if(isset($cart[$id])) {
                unset($cart[$id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
        
    }
}


    // public function addToCart1(Request $request)
    // {
    //     $prod_id = $request->get('productId');
    //     $quant = $request->get('quantity');
    //     $id=$prod_id;
    //     //dd($prod_id );



    //     $cart = session()->get('cart');
    //     $return = DB::select("select * from products where productId= '$prod_id'  ");
    //     //dd($return);
    //     //  dd($cart);
    //     if(!$cart) {

    //         $cart = [
    //             $prod_id => [
    //                 "product_id" => $prod_id,
    //                 "name" =>$return[0]->name,
    //                 "description" =>$return[0]->description,
    //                 "quantity" => $quant,
    //                 "price" => $return[0]->price
    //             ]
    //         ];

    //         session()->put('cart', $cart);

    //         //dd($cart);//return redirect()->back()->with('success', 'Product added to cart successfully!');
    //     }

    //     if(isset($cart[$prod_id])) {

    //         $currentQ = $cart[$prod_id]['quantity'];
    //         //dd($currentQ);

    //         $cart[$prod_id]['quantity'] = $currentQ + $quant;

    //         session()->put('cart', $cart);

    //         //return redirect()->back()->with('success', 'Product added to cart successfully!');
    //         // dd($cart);
    //     }

    //     // if item not exist in cart then add to cart with quantity = 1
    //     $cart[$prod_id] = [
    //         "product_id" => $prod_id,
    //         "name" =>$return[0]->name,
    //         "description" =>$return[0]->description,
    //         "quantity" => $quant,
    //         "price" => $return[0]->price
    //     ];

    //     session()->put('cart', $cart);

    //     $output['noItems'] = count($cart);
    //     $output['total'] = 0;

    //     $arr=array();
    //     $arr[0]=$output;
 

