<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payfast;
//use Illuminate\Support\Facades\Request;
use Srmklive\PayPal\Services\ExpressCheckout;

class PaypalController extends Controller
{
    public function notify()
    {
        
        return view('notify');
    }

    public function success()
    {
        
        return view('success');
    }
}
