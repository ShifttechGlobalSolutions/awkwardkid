<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCataloguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogues', function (Blueprint $table) {
            $table->id();
            $table->string('ProductCategory');
            $table->string('ProductName');
            $table->text('ProductDescription');
            $table->string('ProductCode');
            $table->double('ProductPrice');
            $table->double('ProductOldPrice');
            $table->double('ProductDiscount');
            $table->integer('StockQuantity');
            $table->string('file_path');
            // $table->text('ProductImage');
            $table->string('ProductPromotionStatus');
            // $table->string('productid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogues');
    }
}
