<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Catalogue;

class CatalogueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catalogue = [
            [
            'ProductCategory' => 'Watch',
            'ProductName' => 'Watch1',
            'ProductDescription' => 'Good watch',
            'ProductCode' => 'W-001',
            'ProductPrice' => 250,
            'ProductOldPrice' => 50,
            'ProductDiscount' => 0,
            'StockQuantity' => 250,
            'file_path' => 'https://images.unsplash.com/photo-1523275335684-37898b6baf30?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=989&q=80',
            'ProductPromotionStatus' => 'Visible',
            'productId' => 003
        ],
        [
            'ProductCategory' => 'Watch',
            'ProductName' => 'Watch2',
            'ProductDescription' => 'Good watch',
            'ProductCode' => 'W-002',
            'ProductPrice' => 250,
            'ProductOldPrice' => 50,
            'ProductDiscount' => 0,
            'StockQuantity' => 250,
            'file_path' => 'https://images.unsplash.com/photo-1523275335684-37898b6baf30?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=989&q=80',
            'ProductPromotionStatus' => 'Visible',
            'productId' => 004
        ],
        [
            'ProductCategory' => 'Watch',
            'ProductName' => 'Watch3',
            'ProductDescription' => 'Good watch',
            'ProductCode' => 'W-003',
            'ProductPrice' => 250,
            'ProductOldPrice' => 50,
            'ProductDiscount' => 0,
            'StockQuantity' => 250,
            'file_path' => 'https://images.unsplash.com/photo-1523275335684-37898b6baf30?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=989&q=80',
            'ProductPromotionStatus' => 'Visible',
            'productId' => 005
        ]
        ];

        foreach ($catalogue as $key => $value) {
            Catalogue::create($value);
        }
    }
}
