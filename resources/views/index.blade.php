@extends("layouts.master")


@section("content")



    <main class="main">
        <div class="intro-slider-container">
            <div class="owl-carousel owl-simple owl-light owl-nav-inside" data-toggle="owl" data-owl-options='{"nav": false}'>
                <div class="intro-slide" style="background-image: url({{'/images/demos/demo-2/slider/1.png'}});">
                    <div class="container intro-content">
                        <h3 class="intro-subtitle">Baby & Kids Clothing</h3><!-- End .h3 intro-subtitle -->
                        <h1 class="intro-title">Biggest Offers<br>This Season.</h1><!-- End .intro-title -->

                        <a href="category.html" class="btn btn-primary">
                            <span>Shop Now</span>
                            <i class="icon-long-arrow-right"></i>
                        </a>
                    </div><!-- End .container intro-content -->
                </div><!-- End .intro-slide -->

                <!-- <div class="intro-slide" style="background-image: url({{'/images/demos/demo-2/slider/3.png'}});"> -->
                    <div class="container intro-content">
                        <h3 class="intro-subtitle">Deals and Promotions</h3><!-- End .h3 intro-subtitle -->
                        <h1 class="intro-title">Boys <br> Shorts & Shirts <br><span class="text-primary"></span></h1><!-- End .intro-title -->

                        <a href="category.html" class="btn btn-primary">
                            <span>Shop Now</span>
                            <i class="icon-long-arrow-right"></i>
                        </a>
                    </div><!-- End .container intro-content -->
                </div><!-- End .intro-slide -->

                <div class="intro-slide" style="background-image: url({{'/images/demos/demo-2/slider/4.png'}});">
                    <div class="container intro-content">
                        <h3 class="intro-subtitle">Boys Clothing</h3><!-- End .h3 intro-subtitle -->
                        <h1 class="intro-title">
                            Make Your Child  <br>Wear The Best.<br>

                        </h1><!-- End .intro-title -->

                        <a href="category.html" class="btn btn-primary">
                            <span>Shop Now</span>
                            <i class="icon-long-arrow-right"></i>
                        </a>
                    </div><!-- End .container intro-content -->
                </div><!-- End .intro-slide -->
            </div><!-- End .owl-carousel owl-simple -->

            <span class="slider-loader text-white"></span><!-- End .slider-loader -->
        </div><!-- End .intro-slider-container -->

{{--        <div class="brands-border owl-carousel owl-simple" data-toggle="owl"--}}
{{--             data-owl-options='{--}}
{{--                    "nav": false,--}}
{{--                    "dots": false,--}}
{{--                    "margin": 0,--}}
{{--                    "loop": false,--}}
{{--                    "responsive": {--}}
{{--                        "0": {--}}
{{--                            "items":2--}}
{{--                        },--}}
{{--                        "420": {--}}
{{--                            "items":3--}}
{{--                        },--}}
{{--                        "600": {--}}
{{--                            "items":4--}}
{{--                        },--}}
{{--                        "900": {--}}
{{--                            "items":5--}}
{{--                        },--}}
{{--                        "1024": {--}}
{{--                            "items":6--}}
{{--                        },--}}
{{--                        "1360": {--}}
{{--                            "items":7--}}
{{--                        }--}}
{{--                    }--}}
{{--                }'>--}}
{{--            <a href="#" class="brand">--}}
{{--                <img src="{{asset('/images/brands/1.png')}}" alt="Brand Name">--}}

{{--            </a>--}}

{{--            <a href="#" class="brand">--}}
{{--                <img src="{{asset('/images/brands/2.png')}}" alt="Brand Name">--}}
{{--            </a>--}}

{{--            <a href="#" class="brand">--}}
{{--                <img src="{{asset('/images/brands/3.png')}}" alt="Brand Name">--}}
{{--            </a>--}}

{{--            <a href="#" class="brand">--}}
{{--                <img src="{{asset('/images/brands/4.png')}}" alt="Brand Name">--}}
{{--            </a>--}}

{{--            <a href="#" class="brand">--}}
{{--                <img src="{{asset('/images/brands/5.png')}}" alt="Brand Name">--}}
{{--            </a>--}}

{{--            <a href="#" class="brand">--}}
{{--                <img src="{{asset('/images/brands/6.png')}}" alt="Brand Name">--}}
{{--            </a>--}}

{{--            <a href="#" class="brand">--}}
{{--                <img src="{{asset('/images/brands/7.png')}}" alt="Brand Name">--}}
{{--            </a>--}}
{{--        </div><!-- End .owl-carousel -->--}}

        <div class="mb-3 mb-lg-5"></div><!-- End .mb-3 mb-lg-5 -->

        <div class="banner-group">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <div class="banner banner-large banner-overlay banner-overlay-light">
                            <a href="#">
                                <img src="{{asset('/images/demos/demo-2/banners/kido2.jpg')}}" alt="Banner">

                            </a>

                            <div class="banner-content banner-content-top">
{{--                                <h4 class="banner-subtitle">Clearence</h4><!-- End .banner-subtitle -->--}}
                                <h3 style="color: white" class="banner-title">The Awkward<br> Boys </h3><!-- End .banner-title -->
                                <div  style="color: white" class="banner-text">from R77.99</div><!-- End .banner-text -->
                                <a href="#" class="btn btn-outline-gray banner-link">Shop Now<i class="icon-long-arrow-right"></i></a>
                            </div><!-- End .banner-content -->
                        </div><!-- End .banner -->
                    </div><!-- End .col-lg-5 -->



                    <div class="col-md-12 col-lg-6">
                        <div class="banner banner-large banner-overlay banner-overlay-light">
                            <a href="#">
                                <img src="{{asset('/images/demos/demo-2/banners/girlie2.jpg')}}" alt="Banner">

                            </a>

                            <div class="banner-content banner-content-top">
                                {{--                                <h4 class="banner-subtitle">Clearence</h4><!-- End .banner-subtitle -->--}}
                                <h3 style="color: white" class="banner-title">The Awkward<br> Girls </h3><!-- End .banner-title -->
                                <div  style="color: white"class="banner-text">from R77.99</div><!-- End .banner-text -->
                                <a href="#" class="btn btn-outline-gray banner-link">Shop Now<i class="icon-long-arrow-right"></i></a>
                            </div><!-- End .banner-content -->
                        </div><!-- End .banner -->
                    </div><!-- End .col-lg-4 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .banner-group -->

        <div class="mb-3"></div><!-- End .mb-6 -->

        <div class="container">
            <ul class="nav nav-pills nav-border-anim nav-big justify-content-center mb-3" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="products-featured-link" data-toggle="tab" href="#products-featured-tab" role="tab" aria-controls="products-featured-tab" aria-selected="true">Featured</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="products-sale-link" data-toggle="tab" href="#products-sale-tab" role="tab" aria-controls="products-sale-tab" aria-selected="false">New Arrivals</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="products-top-link" data-toggle="tab" href="#products-top-tab" role="tab" aria-controls="products-top-tab" aria-selected="false">On Sale</a>
                </li>
            </ul>
        </div><!-- End .container -->

        <div class="container-fluid">
            <div class="tab-content tab-content-carousel">
                <!--Featured-->
                <div class="tab-pane p-0 fade show active" id="products-featured-tab" role="tabpanel" aria-labelledby="products-featured-link">
                    <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl"
                         data-owl-options='{
                                "nav": false,
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":5
                                    },
                                    "1600": {
                                        "items":6,
                                        "nav": true
                                    }
                                }
                            }'>
                        @foreach($products as $product)
                        <div class="product product-11 text-center">

                            <figure class="product-media">
                                <a href="product.html">
                                    <img src="{{asset('images/popup/newsletter/kido.jpg')}}" alt="Product image" class="product-image">
                                </a>

                                <div class="product-action-vertical">
                                    <a href="#" class="btn-product-icon btn-wishlist"><span>add to wishlist</span></a>
                                </div><!-- End .product-action-vertical -->
                            </figure><!-- End .product-media -->

                            <div class="product-body">
                                <h3 class="product-title"><a href="product.html">{{$product->ProductName}}</a></h3><!-- End .product-title -->
                                <div class="product-price">
                                    R{{sprintf('%0.2f', round( $product->ProductPrice+0,0001,2))}}
                                </div><!-- End .product-price -->
                            </div><!-- End .product-body -->
                            <div class="product-action">
                                <a href="{!!url('/productDetails')!!}/{{ $product->productId }}" class="btn-product btn-cart"><span>add to cart</span></a>
                            </div><!-- End .product-action -->

                        </div><!-- End .product -->

                        @endforeach

                         </div><!-- End .owl-carousel -->
                </div><!-- .End .tab-pane -->

                <!--New Arrival-->
                <div class="tab-pane p-0 fade" id="products-sale-tab" role="tabpanel" aria-labelledby="products-sale-link">
                    <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl"
                         data-owl-options='{
                                "nav": false,
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":5
                                    },
                                    "1600": {
                                        "items":6,
                                        "nav": true
                                    }
                                }
                            }'>
                        @foreach($newProducts as $newProducts)
                        <div class="product product-11 text-center">

                                <figure class="product-media">
                                    <span class="product-label label-circle label-new">New</span>
                                    <a href="product.html">
                                        <img src="{{asset('images/popup/newsletter/kido.jpg')}}" alt="Product image" class="product-image">
{{--                                        <img src="{{asset('/images/demos/demo-2/products/product-1-2.jpg')}}" alt="Product image" class="product-image-hover">--}}
                                    </a>

                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                    </div><!-- End .product-action-vertical -->
                                </figure><!-- End .product-media -->

                                    <div class="product-body">
                                        <h3 class="product-title"><a href="product.html">{{$newProducts->ProductName}}</a></h3><!-- End .product-title -->
                                        <div class="product-price">
                                            R{{sprintf('%0.2f', round( $newProducts->ProductPrice+0,0001,2))}}
                                        </div><!-- End .product-price -->
                                    </div><!-- End .product-body -->
                                    <div class="product-action">
                                        <a href="{!!url('/productDetails')!!}/{{ $newProducts->productId }}" class="btn-product btn-cart"><span>add to cart</span></a>
                                    </div><!-- End .product-action -->

                        </div>
                    @endforeach<!-- End .product-action -->
                       <!-- End .product -->

                         </div><!-- End .owl-carousel -->
                </div><!-- .End .tab-pane -->

                <!--onSale-->
                <div class="tab-pane p-0 fade" id="products-top-tab" role="tabpanel" aria-labelledby="products-top-link">
                    <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl"
                         data-owl-options='{
                                "nav": false,
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":5
                                    },
                                    "1600": {
                                        "items":6,
                                        "nav": true
                                    }
                                }
                            }'>
                        @foreach($saleProducts as $saleProducts)
                        <div class="product product-11 text-center">

                                <figure class="product-media">
                                    <span class="product-label label-circle label-sale">Was R{{sprintf('%0.2f', round( $saleProducts->ProductOldPrice+0,0001,2))}}</span>
                                    <a href="product.html">
                                        <img src="{{asset('images/popup/newsletter/kido.jpg')}}" alt="Product image" class="product-image">
                                        {{--                                        <img src="{{asset('/images/demos/demo-2/products/product-1-2.jpg')}}" alt="Product image" class="product-image-hover">--}}
                                    </a>

                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                    </div><!-- End .product-action-vertical -->
                                </figure><!-- End .product-media -->

                                <div class="product-body">
                                    <h3 class="product-title"><a href="product.html">{{$saleProducts->ProductName}}</a></h3><!-- End .product-title -->
                                    <div class="product-price">
                                        R{{sprintf('%0.2f', round( $saleProducts->ProductPrice+0,0001,2))}}
                                    </div><!-- End .product-price -->
                                </div><!-- End .product-body -->
                                <div class="product-action">
                                    <a href="{!!url('/productDetails')!!}/{{ $saleProducts->productId }}" class="btn-product btn-cart"><span>add to cart</span></a>
                                </div><!-- End .product-action -->

                        </div>
                        @endforeach
                        </div><!-- End .owl-carousel -->
                </div><!-- .End .tab-pane -->
            </div><!-- End .tab-content -->
        </div><!-- End .container-fluid -->

        <div class="mb-5"></div><!-- End .mb-5 -->

        <div class="bg-light deal-container pt-5 pb-3 mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="col-12">
                            <div class="row no-gutters bg-white newsletter-popup-content">
                                <div class="col-xl-3-5col col-lg-7 banner-content-wrap">
                                    <div class="deal">
                                        <div class="deal-content">
                                                                            <h4>Limited Quantities</h4>
                                                                            <h2>Deals of the Month</h2>
                                                                            <div class="deal-countdown" data-until="+720h"></div><!-- End .deal-countdown -->

                                                                            <a href="" class="btn btn-primary">
                                                                                <span>Shop Now</span><i class="icon-long-arrow-right"></i>
                                                                            </a>
                                                                        </div><!-- End .deal-content -->

                                    </div>
                                </div>
                                <div class="col-xl-2-5col col-lg-5 ">
                                    <img src="{{asset('images/popup/newsletter/kido.jpg')}}" class="newsletter-img" alt="newsletter">
                                </div>
                            </div>
                        </div>
                    </div><!-- End .col-lg-9 -->


                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .bg-light -->

        <div class="mb-6"></div><!-- End .mb-6 -->

        <div class="container">
            <div class="heading heading-center mb-3">
                <h2 class="title">Product Ranges</h2><!-- End .title -->

                <ul class="nav nav-pills nav-border-anim justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="top-all-link" data-toggle="tab" href="#top-all-tab" role="tab" aria-controls="top-all-tab" aria-selected="true">All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="top-fur-link" data-toggle="tab" href="#top-fur-tab" role="tab" aria-controls="top-fur-tab" aria-selected="false">Boys</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="top-decor-link" data-toggle="tab" href="#top-decor-tab" role="tab" aria-controls="top-decor-tab" aria-selected="false">Girls</a>
                    </li>

                </ul>
            </div><!-- End .heading -->

            <div class="tab-content">
                <div class="tab-pane p-0 fade show active" id="top-all-tab" role="tabpanel" aria-labelledby="top-all-link">
                    <div class="products">
                        <div class="row justify-content-center">
                            @foreach($products as $product)
                            <div class="col-6 col-md-4 col-lg-3 col-xl-5col">

                                <div class="product product-11 text-center">

                                        <figure class="product-media">
                                            <span class="product-label label-circle label-new">New</span>
                                            <a href="product.html">
                                                <img src="{{asset('images/popup/newsletter/kido.jpg')}}" alt="Product image" class="product-image">
                                                {{--                                        <img src="{{asset('/images/demos/demo-2/products/product-1-2.jpg')}}" alt="Product image" class="product-image-hover">--}}
                                            </a>

                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                            </div><!-- End .product-action-vertical -->
                                        </figure><!-- End .product-media -->

                                        <div class="product-body">
                                            <h3 class="product-title"><a href="product.html">{{$product->ProductName}}</a></h3><!-- End .product-title -->
                                            <div class="product-price">
                                                R{{sprintf('%0.2f', round( $product->ProductPrice+0,0001,2))}}
                                            </div><!-- End .product-price -->
                                        </div><!-- End .product-body -->
                                        <div class="product-action">
                                            <a href="{!!url('/productDetails')!!}/{{ $product->productId }}" class="btn-product btn-cart"><span>add to cart</span></a>
                                        </div><!-- End .product-action -->
                               <!-- End .product-action -->
                                </div>
                           <!-- End .product -->
                            </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->
                        @endforeach
                            <!-- End .col-sm-6 col-md-4 col-lg-3 -->
                        </div><!-- End .row -->
                    </div><!-- End .products -->
                </div><!-- .End .tab-pane -->
                <div class="tab-pane p-0 fade" id="top-fur-tab" role="tabpanel" aria-labelledby="top-fur-link">
                    <div class="products">
                        <div class="row justify-content-center">
                            @foreach($boysProducts as $boysProducts)
                            <div class="col-6 col-md-4 col-lg-3 col-xl-5col">

                                <div class="product product-11 text-center">

                                        <figure class="product-media">
                                            <span class="product-label label-circle label-new">New</span>
                                            <a href="product.html">
                                                <img src="{{asset('images/popup/newsletter/kido.jpg')}}" alt="Product image" class="product-image">
                                                {{--                                        <img src="{{asset('/images/demos/demo-2/products/product-1-2.jpg')}}" alt="Product image" class="product-image-hover">--}}
                                            </a>

                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                            </div><!-- End .product-action-vertical -->
                                        </figure><!-- End .product-media -->

                                        <div class="product-body">
                                            <h3 class="product-title"><a href="product.html">{{$boysProducts->ProductName}}</a></h3><!-- End .product-title -->
                                            <div class="product-price">
                                                R{{sprintf('%0.2f', round( $boysProducts->ProductPrice+0,0001,2))}}
                                            </div><!-- End .product-price -->
                                        </div><!-- End .product-body -->
                                        <div class="product-action">
                                            <a href="{!!url('/productDetails')!!}/{{ $boysProducts->productId }}" class="btn-product btn-cart"><span>add to cart</span></a>
                                        </div><!-- End .product-action -->
                               <!-- End .product-action -->
                                </div>
                            <!-- End .product -->
                            </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->
                        @endforeach
                            <!-- End .col-sm-6 col-md-4 col-lg-3 -->
                        </div><!-- End .row -->
                    </div><!-- End .products -->
                </div><!-- .End .tab-pane -->
                <div class="tab-pane p-0 fade" id="top-decor-tab" role="tabpanel" aria-labelledby="top-decor-link">
                    <div class="products">
                        <div class="row justify-content-center">
                            @foreach($girlsProducts as $girlsProducts)
                            <div class="col-6 col-md-4 col-lg-3 col-xl-5col">

                                <div class="product product-11 text-center">

                                        <figure class="product-media">
                                            <span class="product-label label-circle label-new">New</span>
                                            <a href="product.html">
                                                <img src="{{asset('images/popup/newsletter/kido.jpg')}}" alt="Product image" class="product-image">
                                                {{--                                        <img src="{{asset('/images/demos/demo-2/products/product-1-2.jpg')}}" alt="Product image" class="product-image-hover">--}}
                                            </a>

                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                            </div><!-- End .product-action-vertical -->
                                        </figure><!-- End .product-media -->

                                        <div class="product-body">
                                            <h3 class="product-title"><a href="product.html">{{$girlsProducts->ProductName}}</a></h3><!-- End .product-title -->
                                            <div class="product-price">
                                                R{{sprintf('%0.2f', round( $girlsProducts->ProductPrice+0,0001,2))}}
                                            </div><!-- End .product-price -->
                                        </div><!-- End .product-body -->
                                        <div class="product-action">
                                            <a href="{!!url('/productDetails')!!}/{{ $girlsProducts->productId }}" class="btn-product btn-cart"><span>add to cart</span></a>
                                        </div><!-- End .product-action -->
                               <!-- End .product-action -->
                                </div>
                         <!-- End .product -->
                            </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->
                        @endforeach
                            <!-- End .col-sm-6 col-md-4 col-lg-3 -->
                        </div><!-- End .row -->
                    </div><!-- End .products -->
                </div><!-- .End .tab-pane -->
                </div><!-- End .tab-content -->
        </div><!-- End .container -->

        <div class="container">
            <hr class="mt-1 mb-6">
        </div><!-- End .container -->

 </main><!-- End .main -->
@endsection
