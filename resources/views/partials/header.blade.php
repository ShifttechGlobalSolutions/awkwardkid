<!-- <link rel="stylesheet" href="/admin/plugins/bootstrap/css/bootstrap.min.css"> -->
<header class="header header-2 header-intro-clearance">
    <div class="header-top">
        <div class="container">
            <div class="header-left">
                <p>Special collection already available.</p><a href="#">&nbsp;Read more ...</a>
            </div><!-- End .header-left -->

            <div class="header-right">

                <ul class="top-menu">
                    <li>
                        <a href="#">Links</a>
                        <ul>
                            <li>
                                <div class="header-dropdown">
                                    <a href="#">Rand</a>
{{--                                    <div class="header-menu">--}}
{{--                                        <ul>--}}
{{--                                            <li><a href="#">Rand</a></li>--}}
{{--                                            <li><a href="#">Usd</a></li>--}}
{{--                                        </ul>--}}
{{--                                    </div><!-- End .header-menu -->--}}
                                </div>
                            </li>
                            <li>
                                <div class="header-dropdown">
                                    <a href="#">English</a>
                                    <div class="header-menu">
                                        <ul>
                                            <li><a href="#">English</a></li>
                                            <li><a href="#">Afrikaans</a></li>

                                        </ul>
                                    </div><!-- End .header-menu -->
                                </div>
                            </li>
                            <li><a href="#signin-modal" data-toggle="modal">Sign in / Sign up</a></li>
                        </ul>
                    </li>
                </ul><!-- End .top-menu -->
            </div><!-- End .header-right -->

        </div><!-- End .container -->
    </div><!-- End .header-top -->

    <div class="header-middle">
        <div class="container">
            <div class="header-left">
                <button class="mobile-menu-toggler">
                    <span class="sr-only">Toggle mobile menu</span>
                    <i class="icon-bars"></i>
                </button>

                <a href="index.html" class="logo">
                    <img src="{{asset('/images/kidlogo.jpg" alt="AwkwardKid Logo" width="105" height="25"')}}">
                </a>
            </div><!-- End .header-left -->

            <div class="header-center">
                <div class="header-search header-search-extended header-search-visible header-search-no-radius d-none d-lg-block">
                    <a href="#" class="search-toggle" role="button"><i class="icon-search"></i></a>
                    <form action="#" method="get">
                        <div class="header-search-wrapper search-wrapper-wide">
                            <label for="q" class="sr-only">Search</label>
                            <input type="search" class="form-control" name="q" id="q" placeholder="Search product ..." required>
                            <button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
                        </div><!-- End .header-search-wrapper -->
                    </form>
                </div><!-- End .header-search -->
            </div>

            <div class="header-right">
                <div class="account">
                    <a href="{{ url('/admin/index') }}" title="My account">
                        <div class="icon">
                            <i class="icon-user"></i>
                        </div>
                        <p>Account</p>
                    </a>
                </div><!-- End .compare-dropdown -->

                <?php $cart = session()->get('cart'); ?>
                <div class="dropdown cart-dropdown">
                    <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                        <div class="icon">
                            <i class="icon-shopping-cart"></i>
                            <span class="cart-count"><?php echo collect($cart)->count() ?></span>
                        </div>
                        <p>Cart</p>
                    </a>
                    

                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-cart-products">

                        @php ($total = 0) 
                        @if(session('cart'))
                        @foreach(session('cart') as $id => $details)
                        @php ($total += $details['price'] * $details['quantity'])
                       
                            <div data-id="{{ $id }}" class="product">
                                <div class="product-cart-details">
                                    <h4 class="product-title">
                                        <a href="product.html">{{ $details['name'] }}</a>
                                    </h4>

                                    <span class="cart-product-info">
                                                <span class="cart-product-qty">{{ $details['quantity'] }}</span>
                                                x {{ $details['price'] }}
                                            </span>
                                </div><!-- End .product-cart-details -->

                                <figure class="product-image-container">
                                    <a href="product.html" class="product-image">
                                        <img src="{{asset('/images/products/cart/product-1.jpg" alt="product"')}}">
                                    </a>
                                </figure>
                                
                                <a href="sdsd" class="btn-remove" title="Remove Product"><i class="icon-close"></i></a>
                                
                            </div><!-- End .product -->

                            @endforeach
                            @endif

                        <div class="dropdown-cart-total">
                            <span>Total</span>

                            <span class="cart-total-price">{{ $total}}</span>
                        </div><!-- End .dropdown-cart-total -->

                        <div class="dropdown-cart-action">
                            <a  href="" class="btn btn-primary" data-target="#updateCartModal" data-toggle="modal">View Cart</a>
                            <a href="/checkout" class="btn btn-outline-primary-2"><span>Checkout</span><i class="icon-long-arrow-right"></i></a>
                        </div><!-- End .dropdown-cart-total -->
                    </div><!-- End .dropdown-menu -->
                   
                </div><!-- End .cart-dropdown -->
            </div><!-- End .header-right -->
        </div><!-- End .container -->
    </div><!-- End .header-middle -->

    <div class="header-bottom sticky-header">
        <div class="container">
            <div class="header-left">
                <div class="dropdown category-dropdown">
                    <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static" title="Browse Categories">
                        Browse Categories
                    </a>

                    <div class="dropdown-menu">
                        <nav class="side-nav">
                            <ul class="menu-vertical sf-arrows">

                                <li><a href="#">Awkward Boys</a></li>
                                <li><a href="#">Awkward Girls</a></li>

                            </ul><!-- End .menu-vertical -->
                        </nav><!-- End .side-nav -->
                    </div><!-- End .dropdown-menu -->
                </div><!-- End .category-dropdown -->
            </div><!-- End .header-left -->

            <div class="header-center">
                <nav class="main-nav">
                    <ul class="menu sf-arrows">
                        <li class="megamenu-container active">
                            <a href="index.html" class="sf-with-ul">Awkward Boys Clothing</a>


                        </li>

                        <li class="megamenu-container active">
                            <a href="index.html" class="sf-with-ul">Awkward Girls Clothing</a>


                        </li>

                        <li class="megamenu-container active">
                            <a href="index.html" class="sf-with-ul">Awkward Sale</a>


                        </li>

                    </ul><!-- End .menu -->
                </nav><!-- End .main-nav -->
            </div><!-- End .header-center -->

            <div class="header-right">
                <i class="la la-lightbulb-o"></i><p>Clearance<span class="highlight">&nbsp;Up to 30% Off</span></p>
            </div>
        </div><!-- End .container -->
    </div><!-- End .header-bottom -->

     
    <!-- ends Edit Modal Form -->

</header><!-- End .header -->

