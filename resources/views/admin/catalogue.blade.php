@extends("layouts.admin_master")
@section("content")
    <div class="page-wrapper">
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Product Catalogue</h3>
                    </div>
                    <div class="col-auto text-right">
                        <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search">
                            <i class="fas fa-filter"></i>
                        </a>

                        <a href="add-category.html" class="btn btn-primary "  data-target="#importsCatalogueModal" data-toggle="modal">
{{--                            <i class="fas fa-plus">--}}
                                Import Products</i>
                        </a>

                        <a href="add-category.html" class="btn btn-primary "  data-target="#productModal" data-toggle="modal">
{{--                            <i class="fas fa-plus">--}}
                                Add Product</i>
                        </a>
                    </div>


                </div>
            </div>
            <!-- /Page Header -->

            <!-- Search Filter -->
            <div class="card filter-card" id="filter_inputs">
                <div class="card-body pb-0">
                    <form action="#" method="post">
                        <div class="row filter-row">
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control select">
                                        <option>Select category</option>
                                        <option>Automobile</option>
                                        <option>Construction</option>
                                        <option>Interior</option>
                                        <option>Cleaning</option>
                                        <option>Electrical</option>
                                        <option>Carpentry</option>
                                        <option>Computer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>From Date</label>
                                    <div class="cal-icon">
                                        <input class="form-control datetimepicker" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>To Date</label>
                                    <div class="cal-icon">
                                        <input class="form-control datetimepicker" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /Search Filter -->

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-center mb-0 datatable">
                                   <thead>
                                    <tr>
                                        <th>#</th>
{{--                                        <th>Id</th>--}}
                                        <th>Product Image</th>
                                        <th>Product </th>
                                        <th>Category</th>
                                        <th>Stock </th>
                                        <th>Price </th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($catalogue as $catalogues)
                                    <tr>
                                        <td>{{$catalogues['id']}}</td>
                                        <td>{{$catalogues['ProductImage']}}</td>
                                        <td>{{$catalogues['ProductName']}}</td>
                                        <td>{{$catalogues['ProductCategory']}}</td>
                                        <td>{{$catalogues['StockQuantity']}}</td>
                                        <td>{{$catalogues['ProductPrice']}}</td>


                                       <td class="text-right">
                                            <a href="{{'/admin/catalogue/edit/'.$catalogues['id']}}" class="btn btn-sm bg-success-light mr-2" data-target="#editProductModal" data-toggle="modal">	<i class="far fa-edit mr-1"></i> Edit</a>
                                            <a href="{{'/admin/catalogue/delete/'.$catalogues['id']}}" class="btn btn-sm bg-danger-light mr-2">	<i class="fa fa-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="productModal" class="modal fade" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">

                    <div class="modal-header">
                    <h5 class="modal-title">Add Product</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif

                    <form class="needs-validation" method="POST" action="/addCatalogue"  enctype="multipart/form-data" novalidate>
                        @csrf
                        <div class="form-group row ">

                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="productName" name="productName" placeholder="ProductName" required>
                            </div>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="productCode" name="productCode" placeholder="Product Code" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <select class="custom-select" id="productCategory" name="productCategory" required>
                                    <option value="">Product Category</option>
                                    <option value="Boys">Boys</option>
                                    <option value="Girls">Girls</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <select class="custom-select" id="productPromotionStatus" name="productPromotionStatus" required>
                                    <option value=""> Status</option>
                                    <option value="Sale">Sale</option>
                                    <option value="12">New</option>
                                </select>
                            </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="productPrice" name="productPrice" placeholder="Product Price" required>
                            </div>

                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="productOldPrice" name="productOldPrice" placeholder="Product Old Price" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="stockQuantity" name="stockQuantity" placeholder="Quantity" required>
                            </div>

                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="productDiscount" name="productDiscount" placeholder="Discount" required>
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-sm-6">
                                <input type="file" class="form-control" id="productImage" name="file"/>
                            </div>

                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="productid" name="productid" placeholder="Product ID" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <textarea rows="3" type="text" class="form-control" id="productDescription" name="productDescription" placeholder="Product Description" required></textarea>
                            </div>
                     </div>

                        <hr>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save Product</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- edit Modal form -->
    <div id="editProductModal" class="modal fade" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">

                    <div class="modal-header">
                    <h5 class="modal-title">Update Product</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif

                    <form class="needs-validation" method="POST" action="/addCatalogue"  enctype="multipart/form-data" novalidate>
                        @csrf
                        <div class="form-group row ">

                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="productName" name="productName" placeholder="ProductName" required>
                            </div>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="productCode" name="productCode" placeholder="Product Code" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <select class="custom-select" id="productCategory" name="productCategory" required>
                                    <option value="">Product Category</option>
                                    <option value="Boys">Boys</option>
                                    <option value="Girls">Girls</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <select class="custom-select" id="productPromotionStatus" name="productPromotionStatus" required>
                                    <option value=""> Status</option>
                                    <option value="Sale">Sale</option>
                                    <option value="12">New</option>
                                </select>
                            </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="productPrice" name="productPrice" placeholder="Product Price" required>
                            </div>

                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="productOldPrice" name="productOldPrice" placeholder="Product Old Price" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="stockQuantity" name="stockQuantity" placeholder="Quantity" required>
                            </div>

                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="productDiscount" name="productDiscount" placeholder="Discount" required>
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-sm-6">

                                <input type="file" class="form-control" id="productImage" name="file"/>
                            </div>

                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="productid" name="productid" placeholder="Product ID" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <textarea rows="3" type="text" class="form-control" id="productDescription" name="productDescription" placeholder="Product Description" required></textarea>
                            </div>
                     </div>

                        <hr>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save Product</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ends Edit Modal Form -->

    <!-- Bulk Category Imports modal -->
    <div id="importsCatalogueModal" class="modal fade" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Import Products from an Excel file</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <form class="needs-validation" method="POST" action="/importCatalogue" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">

                            <div class="col-sm-6">
                                <label for="file">Choose a file</label>
                            </div>

                            <div class="col-sm-6">
                                <input type="file" class="form-control" id="file" name="file" required>
                            </div>

                        </div>

                        <hr>

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save Category</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Import modal ends here -->
@endsection
