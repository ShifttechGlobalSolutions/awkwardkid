@extends("layouts.admin_master")
@section("content")
    <div class="page-wrapper">
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Product Categories</h3>
                    </div>
                    <div class="col-auto text-right">
                        <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search">
                            <i class="fas fa-filter"></i>
                        </a>

                        <a href="add-category.html" class="btn btn-primary "  data-target="#importsCategoryModal" data-toggle="modal">
                            {{--                            <i class="fas fa-plus">--}}
                            Import Categories</i>
                        </a>
                        <a href="add-category.html" class="btn btn-primary "  data-target="#categoryModal" data-toggle="modal">
                            {{--                            <i class="fas fa-plus">--}}
                            Add Category</i>
                        </a>



                    </div>


                </div>
            </div>
            <!-- /Page Header -->

            <!-- Search Filter -->
            <div class="card filter-card" id="filter_inputs">
                <div class="card-body pb-0">
                    <form action="#" method="post">
                        <div class="row filter-row">
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control select">
                                        <option>Select category</option>
                                        <option>Automobile</option>
                                        <option>Construction</option>
                                        <option>Interior</option>
                                        <option>Cleaning</option>
                                        <option>Electrical</option>
                                        <option>Carpentry</option>
                                        <option>Computer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>From Date</label>
                                    <div class="cal-icon">
                                        <input class="form-control datetimepicker" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>To Date</label>
                                    <div class="cal-icon">
                                        <input class="form-control datetimepicker" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /Search Filter -->

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-center mb-0 datatable">
                                <thead>
                                    <tr>
                                        <th>#</th>


                                        <th>Category</th>
                                        <th>Category Code</th>
                                        <th>Category Status</th>
                                        <th class="text-right">Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($categories as $category)


                                    <tr>
                                        <td>{{$category['id']}}</td>
                                        <td>{{$category['CategoryName']}}</td>
                                        <td>{{$category['CategoryCode']}}</td>
                                        <td>{{$category['CategoryStatus']}}</td>

                                        <td class="text-right">
                                            <a href="{{'/admin/categories/edit/'.$category['id']}}" class= " btn btn-sm bg-success-light mr-2">	<i class="far fa-edit mr-1"></i> Edit</a>
                                            <a href="{{'/admin/categories/delete/'.$category['id']}}" class= " btn btn-sm bg-danger-light mr-2">	<i class="fa fa-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="categoryModal" class="modal fade" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Add Product Category</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <form class="needs-validation" method="POST" action="/addCategory" >
                        @csrf
                        <div class="form-group row ">

                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="categoryName" name="categoryName" placeholder="Category Name" required>
                            </div>


                        </div>
                        <div class="form-group row">

                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="categoryCode" name="categoryCode" placeholder="Category Code" required>
                            </div>

                            <div class="col-sm-6">
                                <select class="custom-select" id="categoryStatus" name="categoryStatus" required>
                                    <option value=""> Category Status</option>
                                    <option value="Visible">Visible</option>
                                    <option value="Hidden">Hidden</option>
                                </select>
                            </div>

                        </div>

                        <hr>

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save Category</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Bulk Category Imports modal -->
    <div id="importsCategoryModal" class="modal fade" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Import Categories from an Excel file</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <form class="needs-validation" method="POST" action="/importCategory" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">

                            <div class="col-sm-6">
                                <label for="file">Choose a file</label>
                            </div>

                            <div class="col-sm-6">
                                <input type="file" class="form-control" id="file" name="file" required>
                            </div>

                        </div>

                        <hr>

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save Category</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Import modal ends here -->


@endsection
