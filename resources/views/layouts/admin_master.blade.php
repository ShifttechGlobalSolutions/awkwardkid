<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Truelysell | Template</title>

    <!-- Favicons -->
    <link rel="shortcut icon" href="/admin/img/favicon.png">
{{--    href="{{asset('/images/icons/apple-touch-icon.png')}}">--}}

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/admin/plugins/bootstrap/css/bootstrap.min.css">

    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="/admin/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="/admin/plugins/fontawesome/css/all.min.css">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="/admin/css/animate.min.css">

    <!-- Main CSS -->
    <link rel="stylesheet" href="/admin/css/admin.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>

</head>

<body>
@include("partials./admin_header")


@yield('content')


@include("partials./admin_footer")

<!-- jQuery -->
<script src="admin/js/jquery-3.5.0.min.js"></script>

<!-- Bootstrap Core JS -->
<script src="admin/js/popper.min.js"></script>
<script src="admin/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- Datepicker Core JS -->
<script src="admin/js/moment.min.js"></script>
<script src="admin/js/bootstrap-datetimepicker.min.js"></script>

<!-- Slimscroll JS -->
<script src="admin/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="admin/js/bootstrapValidator.min.html"></script>

<!-- Datatables JS -->
<script src="admin/plugins/datatables/datatables.min.js"></script>

<!-- Select2 JS -->
<script src="admin/js/select2.min.js"></script>

<!-- Custom JS -->
<script src="admin/js/admin.js"></script>



</body>
</html>
