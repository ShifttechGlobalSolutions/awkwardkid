<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Molla - Bootstrap eCommerce Template</title>
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Molla - Bootstrap eCommerce Template">
    <meta name="author" content="p-themes">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180"  href="{{asset('/images/icons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/images/icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/images/icons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('/images/icons/site.html')}}">
    <link rel="mask-icon" href="{{asset('/images/icons/safari-pinned-tab.svg" color="#666666"')}}">
    <link rel="shortcut icon" href="{{asset('/images/icons/favicon.ico')}}">
    <meta name="apple-mobile-web-app-title" content="Molla">
    <meta name="application-name" content="Molla">
    <meta name="msapplication-TileColor" content="#cc9966">
    <meta name="msapplication-config" content="{{asset('/images/icons/browserconfig.xml')}}">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="{{asset('/vendor/line-awesome/line-awesome/line-awesome/css/line-awesome.min.css')}}">
    <!-- Plugins CSS File -->
    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/plugins/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('/css/plugins/magnific-popup/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('/css/plugins/jquery.countdown.css')}}">
    <!-- Main CSS File -->
    <link rel="stylesheet" href=" {{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{asset('/css/skins/skin-demo-2.css')}}">
    <link rel="stylesheet" href="{{asset('/css/demos/demo-2.css')}}">

</head>
<body>
@include("partials.header")


@yield('content')


@include("partials.footer")

<!-- Plugins JS File -->
<script src="{{asset('/js/jquery.min.js')}}"></script>
<script src="{{asset('/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('/js/jquery.hoverIntent.min.js')}}"></script>
<script src="{{asset('/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('/js/superfish.min.js')}}"></script>
<script src="{{asset('/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('/js/jquery.plugin.min.js')}}"></script>
<script src="{{asset('/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('/js/jquery.countdown.min.js')}}"></script>
<!-- Main JS File -->
<script src="{{asset('/js/main.js')}}"></script>
<script src="{{asset('/js/demos/demo-2.js')}}"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
  
    // $(".update-cart").change(function (e) {
    //     e.preventDefault();
  
    //     var ele = $(this);
  
    //     $.ajax({
            
    //         method: "patch",
    //         data: {
    //             _token: '{{ csrf_token() }}', 
    //             id: ele.parents("tr").attr("data-id"), 
    //             quantity: ele.parents("tr").find(".quantity").val()
    //         },
    //         success: function (response) {
    //            window.location.reload();
    //         }
    //     });
    // });
  
    $(".btn-remove").click(function (e) {
        e.preventDefault();
  
        var ele = $(this);
  
        if(confirm("Are you sure want to remove?")) {
            $.ajax({
                url: '/removeFromCart/' + ele.parents("div").attr("data-id"),
                method: "POST",
                data: {
                    _token: '{{ csrf_token() }}', 
                    id: ele.parents("div").attr("data-id")
                },
                success: function (response) {
                    window.location.reload();
                }
            });
        }
    });
  
</script>
</body>
