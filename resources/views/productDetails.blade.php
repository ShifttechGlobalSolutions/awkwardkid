@extends("layouts.master")


@section("content")

    <main class="main">
        <nav aria-label="breadcrumb" class="breadcrumb-nav border-0 mb-0">
            <div class="container d-flex align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Products</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Default</li>
                </ol>

                <!-- End .pager-nav -->
            </div><!-- End .container -->
        </nav><!-- End .breadcrumb-nav -->

        <div class="page-content">
            <div class="container">
                <div class="product-details-top">
                    @foreach($selectedProduct as $selected_Product)
                    <div class="row">
                        <div class="col-md-6">
                            <div class="product-gallery product-gallery-vertical">
                                <div class="row">
                                    <figure class="product-main-image">
                                        <img id="product-zoom" src="{{asset('images/popup/newsletter/kido.jpg')}}" data-zoom-image="{{asset('images/popup/newsletter/kido.jpg')}}" alt="product image">
                                           <a href="#" id="btn-product-gallery" class="btn-product-gallery">
                                            <i class="icon-arrows"></i>
                                        </a>
                                    </figure><!-- End .product-main-image -->

                                    <div id="product-zoom-gallery" class="product-image-gallery">
                                        <a class="product-gallery-item active" href="#" data-image="{{asset('images/popup/newsletter/kido.jpg')}}" data-zoom-image="{{asset('images/popup/newsletter/kido.jpg')}}">
                                            <img src="{{asset('images/popup/newsletter/kido.jpg')}}" alt="product side">
                                        </a>

                                        <a class="product-gallery-item" href="#" data-image="{{asset('images/popup/newsletter/kido.jpg')}}" data-zoom-image="{{asset('images/popup/newsletter/kido.jpg')}}">
                                            <img src="{{asset('images/popup/newsletter/kido.jpg')}}" alt="product cross">
                                        </a>

                                        <a class="product-gallery-item" href="#" data-image="{{asset('images/popup/newsletter/kido.jpg')}}" data-zoom-image="{{asset('images/popup/newsletter/kido.jpg')}}">
                                            <img src="{{asset('images/popup/newsletter/kido.jpg')}}" alt="product with model">
                                        </a>

                                        <a class="product-gallery-item" href="#" data-image="{{asset('images/popup/newsletter/kido.jpg')}}" data-zoom-image="{{asset('images/popup/newsletter/kido.jpg')}}">
                                            <img src="{{asset('images/popup/newsletter/kido.jpg')}}" alt="product back">
                                        </a>
                                    </div><!-- End .product-image-gallery -->
                                </div><!-- End .row -->
                            </div><!-- End .product-gallery -->
                        </div><!-- End .col-md-6 -->

                        <div class="col-md-6">
                            <div class="product-details">
                                <h1 class="product-title">{{$selected_Product->productId}}</h1><!-- End .product-title -->

                                <div class="product-content">
                                    <p>{{$selected_Product->ProductName}}</p>
                                </div><!-- End .product-content -->

                                <div class="product-price">
                                    R{{sprintf('%0.2f', round( $selected_Product->ProductPrice+0,0001,2))}}
                                </div><!-- End .product-price -->
                                    <div class="form-row  mt-4">
                                        <div class=" ">
                                            <div class=" ">
                                                <div class="input-group-prepend" style="display: none">
                                                    <button class="btn btn-light" type="button" id="button-plus"> + </button>
                                                </div>
                                                <input type="number" id="Quantity" name="Quantity" class="form-control" min="1" value="1">
                                                <input type="hidden" id="Product_id" name="Product_id" class="form-control" value="{{$selected_Product->productId}}">
                                                <!-- <input type="hidden" id="Product_name" name="Product_name" class="form-control" value="{{$selected_Product->ProductName}}"> -->

                                                <div class="input-group-append" style="display: none">
                                                    <button class="btn btn-light" name="btnQty" type="button" id="button-minus"> &minus; </button>
                                                </div>
                                            </div>
                                        </div> <!-- col.// -->
                                        <div class="form-group col-md product-details-action">
                                            <a href="{{'/addToCart/'.$selected_Product->productId}}" class="btn btn-outline-primary-2">	<i class="fas fa-cart-plus">Add To Cart</i></a>
                                        </div>

                                    <!-- col.// -->
                                    </div><!-- End .product-details -->
                        </div>
                            <div class="product-details-footer">
                                <div class="dropdown-cart-action">
{{--                                    <a href="cart.html" class="btn btn-primary">View Cart</a>--}}
                                    <a href="/" class="btn btn-outline-primary-2"><span>Continue Shopping</span><i class="icon-long-arrow-right"></i></a>
                                </div><!-- End .product-cat -->


                            </div>
                        </div><!-- End .col-md-6 -->
                    </div>
                @endforeach<!-- End .row -->
                </div><!-- End .product-details-top -->


                <h2 class="title text-center mb-4">You May Also Like</h2><!-- End .title text-center -->

                <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl"
                     data-owl-options='{
                            "nav": false,
                            "dots": true,
                            "margin": 20,
                            "loop": false,
                            "responsive": {
                                "0": {
                                    "items":1
                                },
                                "480": {
                                    "items":2
                                },
                                "768": {
                                    "items":3
                                },
                                "992": {
                                    "items":4
                                },
                                "1200": {
                                    "items":4,
                                    "nav": true,
                                    "dots": false
                                }
                            }
                        }'>
                    @foreach($products as $product)
                    <div class="product product-7 text-center">
                        <figure class="product-media">
                            <span class="product-label label-circle label-new">New</span>
                            <a href="product.html">
                                <img src="{{asset('images/popup/newsletter/kido.jpg')}}" alt="Product image" class="product-image">
                                {{--                                        <img src="{{asset('/images/demos/demo-2/products/product-1-2.jpg')}}" alt="Product image" class="product-image-hover">--}}
                            </a>

                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                            </div><!-- End .product-action-vertical -->
                        </figure><!-- End .product-media -->

                        <div class="product-body">
                            <h3 class="product-title"><a href="product.html">{{$product->ProductName}}</a></h3><!-- End .product-title -->
                            <div class="product-price">
                                R{{sprintf('%0.2f', round( $product->ProductPrice+0,0001,2))}}
                            </div><!-- End .product-price -->
                        </div><!-- End .product-body -->
                        <div class="product-action">
                            <a href="{!!url('/productDetails')!!}/{{ $product->productId }}" class="btn-product btn-cart"><span>add to cart</span></a>
                        </div><!-- End .product-body -->
                    </div>
                @endforeach<!-- End .product -->

                   <!-- End .product -->
                </div><!-- End .owl-carousel -->
            </div><!-- End .container -->
        </div><!-- End .page-content -->


       


    </main>

@endsection
