@extends("layouts.master")


<style>
    .container {
        padding: 30px;
        width: 100%;
    }
    .checkout-session{
        display: flex;
        padding: 50px;
        /* justify-content: space-between;
        justify-content: space-around; */
        width: 100%;
    }
    .checkout-session div{
        width: 100%;
    }
    .half-form{
        display: flex;
        justify-content: space-between;
        width: 100%;
    }

    .checkout-totals{
        display: flex;
        justify-content: space-between;
        width: 100%;
    }
    .checkout-table-container{
        margin-left: 25%;
        width: 100%;
    }
    .checkout-table-row-left{
        display: flex;
        justify-content: space-between;
        justify-content: space-around;
    }
    .spacer{
        margin-top: 30px;
    }
    .checkout-table-img{
        width: 80px;
        height: 80px;
    }
    @media only screen and (max-width: 777px){
    
    .checkout-session{
        display: inline;
}
    }
</style>

@section("content")

<div class="container">
@php ($total = 0) 
                @if(session('cart'))
                @foreach(session('cart') as $id => $details)
                @php ($total += $details['price'] * $details['quantity']) 

                @endforeach
                @endif

<?php
/**
 * @param array $data
 * @param null $passPhrase
 * @return string
 */
// function generateSignature($data, $passPhrase = null) {
//     // Create parameter string
//     $pfOutput = '';
//     foreach( $data as $key => $val ) {
//         if($val !== '') {
//             $pfOutput .= $key .'='. urlencode( trim( $val ) ) .'&';
//         }
//     }
//     // Remove last ampersand
//     $getString = substr( $pfOutput, 0, -1 );
//     if( $passPhrase !== null ) {
//         $getString .= '&passphrase='. urlencode( trim( $passPhrase ) );
//     }
//     return md5( $getString );
// }


// Construct variables
$cartTotal = $total + ($total * 0.15);// This amount needs to be sourced from your application
$data = array(
    // Merchant details
    'merchant_id' => '10000100',
    'merchant_key' => '46f0cd694581a',
    'return_url' => 'http://localhost:8080/dbServerCon/return.php',
    'cancel_url' => 'http://localhost:8080/dbServerCon/cancel.php',
    'notify_url' => 'http://localhost:8080/dbServerCon/success.php',
    // 'return_url' => '/success',
    // 'cancel_url' => '/cancel',
    // 'notify_url' => '/notify',
    // Buyer details
    'name_first' => 'First Name',
    'name_last'  => 'Last Name',
    'email_address'=> 'test@test.com',
    // Transaction details
    'm_payment_id' => '1234', //Unique payment ID to pass through to notify_url
    'amount' => number_format( sprintf( '%.2f', $cartTotal ), 2, '.', '' ),
    'item_name' => $details['name']
);

// $signature =  generateSignature($data);
// $data['signature'] = $signature;

// If in testing mode make use of either sandbox.payfast.co.za or www.payfast.co.za
$testingMode = true;
$pfHost = $testingMode ? 'sandbox.payfast.co.za' : 'www.payfast.co.za';
$htmlForm = '<form action="https://'.$pfHost.'/eng/process" method="post">';
foreach($data as $name=> $value)
{
    $htmlForm .= '<input name="'.$name.'" type="hidden" value=\''.$value.'\' />';
}
$htmlForm .= '<div class="form-group">
<label for="email">Email Address</label>
<input type="email" name="email" id="email" class="form-control">
</div>';

$htmlForm .=' <div class="half-form">
<div class="form-group">
    <label for="fname">Firstname</label>
    <input type="text" name="fname" id="fname" class="form-control">
</div>
&nbsp;&nbsp;
<div class="form-group">
    <label for="lname">Lastname</label>
    <input type="text" name="lname" id="lname" class="form-control">
</div>
</div>';

$htmlForm .= ' <div class="form-group">
<label for="adr">Address</label>
<input type="text" name="adr" id="adr" class="form-control">
</div>';

$htmlForm .= '<div class="half-form">
<div class="form-group">
    <label for="city">City</label>
    <input type="text" name="city" id="city" class="form-control">
</div>
&nbsp;&nbsp;
<div class="form-group">
    <label for="province">Province</label>
    <input type="text" name="province" id="province" class="form-control">
</div>
</div>';

$htmlForm .= ' <div class="half-form">
<div class="form-group">
    <label for="code">Postal Code</label>
    <input type="text" name="code" id="code" class="form-control">
</div>
&nbsp;&nbsp;
<div class="form-group">
    <label for="phone">Phone</label>
    <input type="text" name="phone" id="phone" class="form-control">
</div>
</div>

<div class="spacer"></div>
';

$htmlForm .= '<br><hr><br><h5>Payment Details</h5>

<div class="form-group">
    <label for="nam_on_card">Name on Card</label>
    <input type="text" name="nam_on_card" id="nam_on_card" class="form-control">
</div>';

$htmlForm .= '<div class="form-group">
<label for="address">Address</label>
<input type="text" name="address" id="address" class="form-control">
</div>';

$htmlForm .= '<div class="form-group">
<label for="cc_number">Credit Card Number</label>
<input type="text" name="cc_number" id="cc_number" class="form-control">
</div>';

$htmlForm .= '<div class="half-form">
<div class="form-group">
    <label for="expiry">Expiry</label>
    <input type="text" name="expiry" id="expiry" class="form-control">
</div>
&nbsp;&nbsp;
<div class="form-group">
    <label for="cvc_code">CVC Code</label>
    <input type="text" name="cvc_code" id="cvc_code" class="form-control">
</div></div>';

$htmlForm .= '<input type="hidden" name="payment_method" value="mp">';

$htmlForm .= '<input type="submit" class="btn btn-primary full-width" value="Complete Order"></form>';
// echo $htmlForm;
?>


    <h3 class="checkout-heading stylish-heading">Checkout</h3>
    <div class="checkout-session">
        <div>
            <?php echo $htmlForm; ?>
        </div>


        <div class="checkout-table-container">
            <h5>Your Order</h5>
            <hr>

            <div class="checkout-table">

                

            @if(session('cart'))
            @foreach(session('cart') as $id => $details)
             
                <div class="checkout-table-row">
                    <div class="checkout-table-row-left">
                            <div>
                                <img src="images/popup/newsletter/kido.jpg" alt="" class="checkout-table-img">
                            </div>
                            <div class="checkout-item-details">
                                <div class="checkout-table-item">{{ $details['name'] }}</div>
                                <div class="checkout-table-description">{{ $details['description'] }}</div>
                                <div class="checkout-table-price">R. {{ $details['price'] }}</div>
                            </div>
                        

                        <div class="checkout-table-row-right">
                            <div class="checkout-table-quantity">{{ $details['quantity'] }}</div>
                        </div>
                    </div>
                </div>
                <hr>
                @endforeach
                @endif
               
                <div class="checkout-totals">
                    <div class="checkout-totals-left">
                        Subtotals <br>
                        Discount <br>  
                        Tax (V.A.T - 15%)<br>
                        <span class="checkout totals-total">Total</span>  
                    </div>

                   
                    <div class="checkout-totals-right">
                       R. {{$total}}<br>
                      R. 0 <br>  
                        R. {{$total * 0.15}} <br>
                        <span class="checkout totals-total">R. {{$cartTotal}}</span>  
                    </div>
                </div>
                <hr>
            </div>

            
        </div>
    </div>
</div>

@endsection