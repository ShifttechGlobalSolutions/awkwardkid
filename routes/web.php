<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CatalogueController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\MarketingController;
use App\Http\Controllers\AnalyticsController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\ProductDetailsController;
use App\Http\Controllers\CheckoutController;
use app\Http\Controllers\PaypalController;
use App\Models\Catalogue;
use App\Models\Categories;
use App\Models\Checkout;




Route::get('/', function () {
    $allProducts= Catalogue::all();
    $girlsProducts= Catalogue::all()->where('ProductCode',"G001");
    $boysProducts= Catalogue::all()->where('ProductCode',"B001");
    $productsOnSale= Catalogue::all()->where('ProductPromotionStatus',"Sale");
    $newProducts= Catalogue::all()->where('ProductPromotionStatus',12);

    //$categories= Categories::all();
    //dd($newProducts);
    return view('index')
        ->with('products', $allProducts)
        ->with('girlsProducts', $girlsProducts)
        ->with('boysProducts', $boysProducts)
        ->with('saleProducts', $productsOnSale)
        ->with('newProducts', $newProducts);
});

//frontend routes -cart

Route::get('productDetails/{id}', [ProductDetailsController::class, 'productDetails']);
// Route::get('addToCart', [CartController::class, 'addToCart']);
Route::get('cart', [CartController::class,'cart']);

Route::get('/addToCart/{id}', [CartController::class, 'addToCart']);
Route::get('/updateCart', [CartController::class, 'update']);
Route::post('/removeFromCart/{id}', [CartController::class, 'remove']);
Route::get('/checkout', [CheckoutController::class, 'index']);


//backend admin routes
Route::get('admin/index',[DashboardController::class, 'index']);
Route::get('admin/catalogue',[CatalogueController::class, 'index']);
Route::get('admin/categories',[CategoriesController::class, 'index']);
Route::get('admin/orders',[OrdersController::class, 'index']);
Route::get('admin/marketing',[MarketingController::class, 'index']);
Route::get('admin/analytics',[AnalyticsController::class, 'index']);
Route::get('admin/customers',[CustomersController::class, 'index']);

// product Category routes
Route::post('/addCategory',[CategoriesController::class, 'store']);
Route::get('admin/categories/delete/{id}',[CategoriesController::class, 'destroy']);
Route::get('admin/categories/edit/{id}',[CategoriesController::class, 'edit']);
Route::post('/importCategory', [CategoriesController::class, 'import']);


// Product Catalogue Routes
Route::post('/addCatalogue',[CatalogueController::class, 'store']);
Route::post('/editCatalogue/{id}',[CatalogueController::class, 'update']);
Route::get('admin/catalogue/delete/{id}',[CatalogueController::class, 'destroy']);
Route::post('/importCatalogue', [CatalogueController::class, 'import']);
// Route::get('admin/catalogue/edit/{id}',[CatalogueController::class, 'edit']);


//Paypal Payment Routes
Route::get('/notify', [PaypalController::class,'notify']);
Route::get('/success', [PaypalController::class. 'success']);
Route::get('/cancel', [PaypalController::class, 'cancel']);




Auth::routes(['verify' => true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware(["verified"]);
